<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bonusreason".
 *
 * @property integer $Id
 * @property string $name
 *
 * @property Bonus[] $bonuses
 */
class Bonusreason extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonusreason';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonuses()
    {
        return $this->hasMany(Bonus::className(), ['reasonId' => 'Id']);
    }
	public static function getBonusreason()
	{
		$allBonusreason = self::find()->all();
		$allBonusreasonArray = ArrayHelper::
					map($allBonusreason, 'Id', 'name');
		return $allBonusreasonArray;						
	}
	public function getBonusreasonItem()
	{
      return $this->hasOne(Bonusreason::className(), ['reasonId' => 'Id']);
	}
}
